import Vue from 'vue'
import VueRouter from 'vue-router';
import GameMenu from './components/GameMenu'
import GamePlay from './components/GamePlay'
import GameOver from './components/GameOver'

Vue.use(VueRouter)

export const router = new VueRouter({
    routes: [{
            path: '',
            component: GameMenu
        },
        {
            path: '/GamePlay/:qamount',
            props: true,
            component: GamePlay
        },
        {
            path: '/GameOver/:score',
            props: true,
            component: GameOver
        },
    ]
})